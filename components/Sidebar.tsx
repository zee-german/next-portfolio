import { IoLogoMastodon as Mastodon } from "react-icons/io5";
import {
  RiPixelfedFill as Pixelfed,
  RiGitlabFill as GitLab,
} from "react-icons/ri";
import { GoLocation } from "react-icons/go";
import { AiOutlineFilePdf as PDF } from "react-icons/ai";

const Sidebar = () => {
  return (
    <div>
      <img
        src="/avatar.jpg"
        alt="avatar"
        className="w-32 h-32 mx-auto rounded-full "
      />
      <h3 className="my-4 text-3xl font-bold font-kaushan">
        Fiona <span className="text-pink-500">"Kweery"</span> Lutzenberger
      </h3>
      <p className="px-2 py-2 my-3 bg-purple-200 rounded-full">
        Full Stack Developer
      </p>
      <a
        href="#"
        download="#"
        className="flex items-center justify-center px-2 py-2 my-3 bg-purple-200 rounded-full"
      >
        <PDF className="w-6 h-6" />
        Download Resume
      </a>

      <div className="flex justify-around w-9/12 mx-auto my-5 text-purple-500 md:w-full">
        <a href="#" className="w-8 h-8 cursor-pointer">
          <GitLab className="w-8 h-8" />
        </a>
        <a href="#" className="w-8 h-8 cursor-pointer">
          <Mastodon className="w-8 h-8" />
        </a>
        <a href="#" className="w-8 h-8 cursor-pointer">
          <Pixelfed className="w-8 h-8" />
        </a>
      </div>

      <div
        className="py-4 my-5 bg-purple-200"
        style={{ marginLeft: "-1rem", marginRight: "-1rem" }}
      >
        <div className="flex items-center justify-center space-x-2">
          <GoLocation /> <span>Cologne, Germany</span>
        </div>
        <p className="my-2">user@example.com</p>
        <p className="my-2">0800-call-me-now</p>
      </div>
      <button className="w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-purple-600 to-pink-500 focus:outline-none">
        Email
      </button>
      <button className="w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-purple-600 to-pink-500 focus:outline-none">
        toggle theme
      </button>
    </div>
  );
};

export default Sidebar;
