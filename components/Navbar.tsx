import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const pages = [
  { name: "About", link: "/" },
  { name: "Resume", link: "/resume" },
  { name: "Portfolio", link: "/portfolio" },
];

const Navbar = () => {
  const [activeItem, setActiveItem] = useState("");

  const { pathname } = useRouter();

  useEffect(() => {
    const currentPage = pages.filter((page) => page.link === pathname);
    setActiveItem(currentPage[0].name);
  }, []);

  const pageList = pages.filter((page) => page.name !== activeItem);

  return (
    <div className="flex justify-between px-5 py-3 my-3">
      <span className="text-xl font-bold text-purple-600 border-b-4 border-purple-500 md:text-2xl">
        {activeItem}
      </span>
      <div className="flex space-x-3 text-xl text-gray-700">
        {pageList.map((item) => {
          return (
            <Link href={item.link} key={item.name}>
              <a>
                <span
                  onClick={() => setActiveItem(item.name)}
                  className="hover:text-purple-600"
                >
                  {item.name}
                </span>
              </a>
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Navbar;
